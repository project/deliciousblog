<?php

/**
 * @file
 * The main module file for the Delicious blog module.
 */

/**
 * Implementation of hook_perm().
 */
function deliciousblog_perm() {
  return array(
    'create delicious blog',
  );
}

/**
 * Implementation of hook_theme().
 */
function deliciousblog_theme() {
  return array(
    'deliciousblog_node_content' => array(
      'arguments' => array(
        'content' => NULL,
      ),
    ),
  );
}

/**
 * Implementation of hook_cron().
 */
function deliciousblog_cron() {
  _deliciousblog_refresh();
}

/**
 * Implementation of hook_requirements().
 */
function deliciousblog_requirements($phase) {
  $requirements = array();

  // Ensure translations don't break at install time.
  $t = get_t();

  if ($phase == 'runtime') {
    $path = drupal_get_path('module', 'deliciousblog');
    if (!file_exists($path .'/simplepie.inc')) {
      $requirements['simplepie'] = array(
        'title' => $t('Delicious blog'),
        'value' => $t('SimplePie library missing.'),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('The SimplePie library is missing.'),
      );
    }
  }

  return $requirements;
}

/**
 * Implementation of hook_menu().
 */
function deliciousblog_menu() {
  $items = array();

  $items['user/%user/deliciousblog'] = array(
    'title' => 'Delicious blog',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deliciousblog_userform', 1),
    'access arguments' => array('create delicious blog'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  return $items;
}

/**
 * Form callback: User configuration form.
 */
function deliciousblog_userform(&$form_state, $uid) {
  $userinfo = NULL;
  $uid = $uid->uid;

  $count = db_result(db_query("SELECT COUNT(*) FROM {deliciousblog} WHERE uid = %d", $uid));
  if ($count == 1) {
    $userinfo = db_fetch_object(db_query("SELECT * FROM {deliciousblog} WHERE uid = %d", $uid));
  }

  $node_types = _deliciousblog_get_node_types();
  $node_type_default = (isset($node_types['blog'])) ? 'blog' : '';

  $form = array();

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );

  if ($userinfo != NULL) {
    $form['dbid'] = array(
      '#type' => 'hidden',
      '#value' => $userinfo->dbid,
    );
  }

  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Enable or disable the retrieval of Delicious.com bookmarks.'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->enabled : 0,
    '#return_value' => 1,
  );

  $form['feed_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Delicious.com feed URL'),
    '#description' => t('The URL of the XML feed of your Delicious.com bookmarks.'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->feed_url : '',
    '#size' => 40,
  );

  $form['post_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Post time'),
    '#description' => t('Time of day (0 - 23) the post should be made.'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->post_time : 0,
    '#size' => 2,
  );

  $form['node_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node information'),
    '#description' => t('Define the node type which has to be created and the default options for the node.'),
  );

  $form['node_info']['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type'),
    '#description' => t('The node type to create.'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->node_type : '',
    '#options' => $node_types,
  );

  $form['node_info']['node_status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->node_status : 1,
    '#return_value' => 1,
  );

  $form['node_info']['node_promote'] = array(
    '#type' => 'checkbox',
    '#title' => t('Promoted to front page'),
    '#default_value' => ($userinfo != NULL) ? $userinfo->node_promote : 1,
    '#return_value' => 1,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 */
function deliciousblog_userform_validate($form, &$form_state) {
  $values = $form_state['values'];

  // Check whether the post_time is valid.
  if (!is_numeric($values['post_time'])) {
    form_set_error('post_time', t('The given post time is not a numerical value.'));
  }
  else {
    $post_time = intval($values['post_time']);
    if (($post_time < 0) || ($post_time > 23)) {
      form_set_error('post_time', t('The given post time is not valid.'));
    }
  }

  // Check whether a feed URL is given if functionality is enabled.
  if ((intval($values['enabled']) == 1) && !isset($values['feed_url'])) {
    form_set_error('feed_url', t('Missing Delicious.com feed URL.'));
  }
}

/**
 * Implementation of hook_submit().
 */
function deliciousblog_userform_submit($form, &$form_state) {
  $values = $form_state['values'];

  $data = new stdClass();
  $data->uid = $values['uid'];
  $data->enabled = $values['enabled'];
  $data->post_time = $values['post_time'];
  $data->feed_url = $values['feed_url'];
  $data->node_type = $values['node_type'];
  $data->node_status = $values['node_status'];
  $data->node_promote = $values['node_promote'];

  // @todo: See if below can be improved.

  $count = db_result(db_query("SELECT COUNT(*) FROM {deliciousblog} WHERE uid = %d", $values['uid']));
  if ($count == 1) {
    if (isset($values['dbid'])) {
      $data->dbid = $values['dbid'];
      $return = drupal_write_record('deliciousblog', $data, 'dbid');
    }
    else {
      $return = FALSE;
    }
  }
  else {
    if ($count > 1) {
      // Oops, seems like there is an error so we remove all previous entries.
      db_query("DELETE FROM {deliciousblog} WHERE uid = %d", $values['uid']);
    }

    $return = drupal_write_record('deliciousblog', $data);
  }

  if (($return == SAVED_NEW) || ($return == SAVED_UPDATED)) {
    drupal_set_message('Information saved.');
  }
  else {
    drupal_set_message('Could not save the data.', 'error');
  }
}

/**
 * Get the list of node types available to the user.
 *
 * @return
 *   A list of available node types.
 */
function _deliciousblog_get_node_types() {
  $types = array();

  foreach (node_get_types('names') as $type => $name) {
    if (node_access('create', $type)) {
      $types[$type] = $name;
    }
  }

  return $types;
}

function _deliciousblog_refresh() {
  // Initialize SimplePie.
  // We do this here because we only want to initialize it once.
  include_once './'. drupal_get_path('module', 'deliciousblog') .'/simplepie.inc';

  $result = db_query("SELECT * FROM {deliciousblog}");
  while ($feed = db_fetch_object($result)) {
    _deliciousblog_process_feed($feed);
  }
}

/**
 * Process the Delicious.com feed.
 *
 * @param $feed
 *   The feed object containing all feed information.
 */
function _deliciousblog_process_feed($feed) {
  $now = time();

  if (($now >= ($feed->last_posted + 86400)) && (intval(date('h', $now)) >= $feed->post_time)) {
    $simplepie = new SimplePie();
    $simplepie->enable_cache(FALSE);
    $simplepie->set_timeout(15);
    $simplepie->set_stupidly_fast(TRUE); // We use Drupal input formats.
    $simplepie->set_feed_url($feed->feed_url);

    $success = $simplepie->init();
    if ($success && $simplepie->data) {
      $feed_link = $simplepie->get_link();
      $results = array();

      for ($i = 0, $j = $simplepie->get_item_quantity(); $i < $j; $i++) {
        $item = $simplepie->get_item($i);

        $result_item = array();
        if ($item->get_date('U') > $feed->last_posted) {
          $result_item['title'] = strip_tags(decode_entities($item->get_title()));
          $result_item['link'] = $item->get_permalink();
          $result_item['description'] = $item->get_description();

          $categories = $item->get_categories();
          if (count($categories) > 0) {
            foreach ($categories as $category) {
              $result_item['tags'][] = array(
                'link' => $feed_link .'/'. $category->get_label(),
                'label' => $category->get_label(),
              );
            }
          }

          $results[] = $result_item;
        }
      }

      db_query("UPDATE {deliciousblog} SET last_posted = %d WHERE uid = %d", ($now - 600), $feed->uid);
      if (count($results) > 0) {
        _deliciousblog_create_node($feed, $results);
      }
    }
  }
}

/**
 * Create the actual node.
 *
 * @param $feed
 *   The feed object containing all feed information.
 * @param $content
 *   A list of all items which need to be posted.
 */
function _deliciousblog_create_node($feed, $content) {
  $account = user_load($feed->uid);

  $node = array(
    'type' => $feed->node_type,
    'promote' => $feed->node_promote,
    'comment' => 2, // @todo Create an option for this?
    'status' => $feed->node_status,
    'uid' => $feed->uid,
    'name' => $account->name,
    'title' => t('Links for !date', array('!date' => format_date(time(), 'custom', 'Y-m-d'))),
    'body' => theme('deliciousblog_node_content', $content),
    'date' => format_date(mktime($feed->post_time, 0, 0), 'custom', 'Y-m-d H:i:s O'),
    'format' => FILTER_FORMAT_DEFAULT,
  );

  if (user_access('create '. $feed->node_type .' content', $account)) {
    node_invoke_nodeapi($node, 'insert');
    node_validate($node);
    if ($errors = form_get_errors()) {
      watchdog('delicious blog', $errors, NULL, WATCHDOG_ERROR);
    }
    else {
      $node = node_submit($node);
      node_save($node);

      if ($node->nid) {
        watchdog('content', '@type: added %title using Delicious Blog.', array('@type' => $node->type, '%title' => $node->title), WATCHDOG_NOTICE, l(t('view'), 'node/'. $node->nid));
      }
    }
  }
}

/**
 * Theme the list of items into a single post.
 *
 * @param $content
 *   The list of feed items.
 *
 * @return
 *   A themed item list.
 */
function theme_deliciousblog_node_content($content) {
  $list = array();
  for ($i = 0, $j = count($content); $i < $j; $i++) {
    $item = $content[$i];

    $output = l($item['title'], $item['link'], array('absolute' => TRUE, 'html' => TRUE)) .'<br />';
    if (isset($item['description'])) {
      $output .= check_plain($item['description']) .'<br />';
    }

    $count_tags = count($item['tags']);
    if ($count_tags > 0) {
      $output .= '(tags: ';
      for ($k = 0; $k < $count_tags; $k++) {
        $output .= l($item['tags'][$k]['label'], $item['tags'][$k]['link'], array('absolute' => TRUE, 'html' => TRUE));
        if ($k < ($count_tags - 1)) {
          $output .= ' ';
        }
      }
      $output .= ')';
    }

    $list[] = $output;
  }

  return theme('item_list', $list);
}
